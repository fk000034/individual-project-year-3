<?php
include_once 'includes/db.inc.php';
?>
<?php
include_once 'includes/verification.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Masters</title>
<link href="Masters/MastersStyle.css" rel="stylesheet" type="text/css">
<link rel="icon" href="Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
	
 
   <!-- Using navigation container as an overlay -->
        <div id="myNav" class="overlay">
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><div class="rotated"><div class="zoom">&times; </div></div></a>
        
        <div class="overlayC">
        <!-- Links to content from menu, when hovered the link is zoomed in, using an animated flow -->
        <a href="Entry Site.php" ><div class="zoom">Entry Site</div></a>
        <a href="FirstYear.php" ><div class="zoom">First Year</div></a>
        <a href="SecondYear.php" ><div class="zoom">Second Year</div></a>
        <a href="ThirdYear.php" ><div class="zoom">Third Year</div></a>
        <a href="Statistics.php" ><div class="zoom">Subject Statistics</div></a>
             <!-- if user is logged on, the nav bar gets this link -->
            <!-- only admins can access this link -->
                        <?php
        if(isset($_SESSION['login'])){
            echo "<a href='Create Module.php' ><div class='zoom'>Add Module</div></a>";
        }
        ?>

    </div>
    </div>
    
   <span style="font-size:70px;cursor:pointer;display:block; height:0px; width: 10px" onclick="openNav()"><div class="clicked"> ☰ </div></span>

<script src="Navigation.js"></script>
 <!-- Page title/header -->
    <h1>Masters</h1>
   
    
    <!-- Background slideshow -->
       <ul class ="slideshow">
    <li><span>Image 1</span></li>
    <li><span>Image 2</span></li>
    
    
</ul>
<!-- Creating the table which contains the records from the database -->
  <div class = "tableContainer">
    <table width="70%", border="5", style="border-collapse:collapse;", align= "center", bgcolor="white">
        <th><strong>Module Code</strong></th>
        <th><strong>Modules</strong></th>
        <th><strong>Sub Category</strong></th>
        <th><strong>Year</strong></th>
        <th><strong>Lecturer</strong></th>
        <!-- higher admins can delete records -->
                           <?php
        if(isset($_SESSION['specialLog'])){
            echo "<th><strong>Delete Record</strong></th>";
        }
        ?>
        <th><strong>More Information</strong></th>
   
<?php
//php code to select all records from the masters table
   $sql = "SELECT*FROM masters;";
   $result = mysqli_query($conn, $sql);
   $check = mysqli_num_rows($result);
     while($row = mysqli_fetch_assoc($result)) { ?>
        <!-- while the query is still valid and connected-->
    <tr>
    <td align="center"><?php echo $row["Module_Code"]; ?></td>
    <td align="center"><?php echo $row["Modules"]; ?></td>
    <td align="center"><?php echo $row["Sub_Category"]; ?></td>
    <td align="center"><?php echo $row["Year_of_Study"]; ?></td>
    <td align="center"><?php echo $row["Lecturer"]; ?></td>
        <!-- Adding the delete button for the higher admins -->
                         <?php
        if(isset($_SESSION['specialLog'])){
            echo "<td align='center'><a href = 'includes/DeleteRecord.php?id4=$row[ID]'> Delete </a></td>";
        }
        ?>     
        <!-- If these module codes exist, theyre able to be accessed and opened for their content pages -->    
     <?php 
        if($row["Module_Code"] == "CSMAD21"){?>
            <td align="center"><a href="Masters/CSMAD21.php">More About CSMAD21</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CSMAI21"){?>
            <td align="center"><a href="Masters/CSMAI21.php">More About CSMAI21</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CSMBD21"){?>
            <td align="center"><a href="Masters/CSMBD21.php">More About CSMBD21</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CSMCC16"){?>
            <td align="center"><a href="Masters/CSMCC16.php">More About CSMCC16</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CSMDE21"){?>
            <td align="center"><a href="Masters/CSMDE21.php">More About CSMDE21</a></td>
           
     <?php } ?>
        
           <?php 
        if($row["Module_Code"] == "CSMDM21"){?>
            <td align="center"><a href="Masters/CSMDM21.php">More About CSMDM21</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CSMIA19"){?>
            <td align="center"><a href="Masters/CSMIA19.php">More About CSMIA19</a></td>
           
     <?php } ?>
        
         <?php 
        if($row["Module_Code"] == "CSMMA21"){?>
            <td align="center"><a href="Masters/CSMMA21.php">More About CSMMA21</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CSMML16"){?>
            <td align="center"><a href="Masters/CSMML16.php">More About CSMML16</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CSMPR21"){?>
            <td align="center"><a href="Masters/CSMPR21.php">More About CSMPR21</a></td>
           
     <?php } ?>
       
        <?php 
        if($row["Module_Code"] == "CSMRS16"){?>
            <td align="center"><a href="Masters/CSMRS16.php">More About CSMRS16</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CSMVI16"){?>
            <td align="center"><a href="Masters/CSMVI16.php">More About CSMVI16</a></td>
           
     <?php } ?>
           <?php 
        if($row["Module_Code"] == "CSMVR16"){?>
            <td align="center"><a href="Masters/CSMVR16.php">More About CS3VR16</a></td>
           
     <?php } ?>
        
    </tr>
            
 <?php } ?> 
        </table>
      </div>

       
 
    </html>
    </body>