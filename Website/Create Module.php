<?php
include_once 'includes/verification.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Opening Page</title>
<link href="Entry.css" rel="stylesheet" type="text/css">
<link rel="icon" href="Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
<!-- PHP code to check if the user is logged in, user has to be logged in to use this page -->
   <?php
if(!isset($_SESSION['login']))
{
    header("location: Login Page.php");
}
?>
    <!-- This PHP code is for security in case a user manages to open this file without a valid log in -->
<script src="Navigation.js"></script>
<!-- Form to enter credentials for new module -->
    <div class="headerContainer">
        <h1>University of Reading Module Site</h1>
        </div>
    <main id = "containerLogin">
        <!-- using form action to proceed to this file after submi -->
        <form action = "includes/InsertDatabase.php" method = "POST">
                  <label>Module</label><input type = "text" name = "Module" class = "box"/><br /><br />
                  <label>Module Code</label><input type = "text" name = "ModuleCode" class = "box" /><br/><br />
                  <label>Category</label><input type = "text" name = "Category" class = "box"/><br /><br />
                  <label>Year of study</label><input type = "text" style="text-transform:uppercase" name = "Year" class = "box" /><br/><br />
                  <label>Lecturer</label><input type = "text" name = "lecturer" class = "box"/><br /><br />
                  <input type = "submit" value = " Submit "/><br />
               </form>
    </main>
    
       <ul class ="slideshow">
    <li><span>Image 1</span></li>
    <li><span>Image 2</span></li>
    <li><span>Image 3</span></li>
    
    
</ul>
  


    </html>
    </body>