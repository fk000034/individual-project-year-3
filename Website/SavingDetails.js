/* jQuery function to help save a file when edited into a database, using ajax */
function saveFile(id) {
    /*let variable to declare a block */
    /*creating text for dialog box to pop up*/
    let text = "Want to save edits?";
    /*if ok is clicked, the edits are saved*/
    if(confirm(text) == true){
    /*html() used to retreive the html contents of the first element in the set*/
    let html = jQuery('#'+id).html();
    /*loading asynchronous data*/
    jQuery.ajax({
        /* Calling to use the php file, with a post method and the data that is used*/
        url: '../includes/SaveDetails.php',
        type:'post',
        /* Getting the async data and the ID where it was edited*/
        data:'html=' + html + '&id=' + id
    })
}
    /*if cancelled, nothing happens therefore edits stay there until page is refreshed*/
    /*giving a chance to change or cancel entirely*/
    else {
        
    }
    
    
}