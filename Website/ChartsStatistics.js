function Charts() {
    /*creating axis for the graphs*/
var Years = [2018, 2019, 2020, 2021, 2022];
var Rank = [45, 45, 56, 59, 46];
var maxStudent = ["Satisfied", "Not Satisfied"];
var studentSatisfaction = [72, 28];
var pieColors = ["black", "blue"];
    /*Line chart is defined and made with the defined axis*/
new Chart("lineChart", {
  type: "line",
  
  data: {
      /*labels being axis*/
    labels: Years,
    datasets: [{
        /*fill is set false as the line graph turns into an area chart if true*/
      fill: false,
        /*Colours for the chart are all initialised, and the data being used is also entered*/
      backgroundColor: "white",
      borderColor: "black",
      lineTension:0,
      pointRadius: 7,
      pointHoverRadius: 10,
      data: Rank,
    }]
  },/*Additional options are added, such as legend, title and a scale*/
  options: {
    legend: {display: false},
    title: {display: true, text: "Subject Satisfaction rate out of 100 Students"},
    scales: {
      yAxes: [{
          ticks: {
              suggestedMin:40,
              suggestedMax:70
                 }
             }],
    }
  }
});
    /*Pie chart initialising, following the same procedure as the line graph*/
  new Chart("pieChartSatisfaction", {
  type: "pie",
  /*Same as above, data is defined, colours and options etc*/
  data: {
    labels: maxStudent,
    datasets: [{
      backgroundColor: pieColors,
      borderColor: "Yellow",
      data: studentSatisfaction,
    }]
  },
  options: {
    legend: {display: true},
    title: {display: true, text: "Subject Satisfaction rate out of 100 Students"}
  }
});
    /*unused chart as more subject data was not found*/
      new Chart("otherstats", {
  type: "pie",
  
  data: {
    labels: maxStudent,
    datasets: [{
      backgroundColor: pieColors,
      borderColor: "Yellow",
      data: studentSatisfaction,
    }]
  },
  options: {
    legend: {display: true},
    title: {display: true, text: "Subject Satisfaction rate out of 100 Students"}
  }
});
    
    
}