<?php
include_once 'includes/db.inc.php';
?>
<?php
include_once 'includes/verification.php';
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Year 3</title>
<link href="Entry.css" rel="stylesheet" type="text/css">
<link rel="icon" href="Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
	
 
   <!-- Using navigation container as an overlay -->
        <div id="myNav" class="overlay">
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><div class="rotated"><div class="zoom">&times; </div></div></a>
        
        <div class="overlayC">
<!-- Links to content from menu, when hovered the link is zoomed in, using an animated flow -->
        <a href="Entry Site.php" ><div class="zoom">Entry Site</div></a>    
    
        <a href="FirstYear.php" ><div class="zoom">First Year</div></a>
        <a href="SecondYear.php" ><div class="zoom">Second Year</div></a>
        <a href="Masters.php" ><div class="zoom">Masters</div></a>
            <a href="Statistics.php" ><div class="zoom">Subject Statistics</div></a>
	<!-- if user is logged on, the nav bar gets this link -->
            <!-- only admins can access this link -->
                        <?php
        if(isset($_SESSION['login'])){
            echo "<a href='Create Module.php' ><div class='zoom'>Add Module</div></a>";
        }
        ?>
    </div>
    </div>
    
   <span style="font-size:70px;cursor:pointer;display:block; height:0px; width: 10px" onclick="openNav()"><div class="clicked"> ☰ </div></span>

<script src="Navigation.js"></script>
  <!-- Page title/header -->
    <h1>Year 3</h1>
   
    
   <!-- Background slideshow -->
       <ul class ="slideshow">
    <li><span>Image 1</span></li>
    <li><span>Image 2</span></li>
    
    
</ul>
<!-- Creating the table which contains the records from the database -->
  <div class = "tableContainer">
    <table width="65%", border="5", style="border-collapse:collapse;", align= "center", bgcolor="white">
        <th><strong>Module Code</strong></th>
        <th><strong>Modules</strong></th>
        <th><strong>Sub Category</strong></th>
        <th><strong>Year</strong></th>
        <th><strong>Lecturer</strong></th>
         <!-- higher admins can delete records -->
                        <?php
        if(isset($_SESSION['specialLog'])){
            echo "<th><strong>Delete Record</strong></th>";
        }
        ?>
        <th><strong>More Information</strong></th>
        
     
    

<?php
//php code to select all records from the year 3 table
   $sql = "SELECT*FROM year3;";
   $result = mysqli_query($conn, $sql);
   $check = mysqli_num_rows($result);
     while($row = mysqli_fetch_assoc($result)) { ?>
        <!-- while the query is still valid and connected-->
    <tr>
    <td align="center"><?php echo $row["Module_Code"]; ?></td>
    <td align="center"><?php echo $row["Modules"]; ?></td>
    <td align="center"><?php echo $row["Sub_Category"]; ?></td>
    <td align="center"><?php echo $row["Year_of_Study"]; ?></td>
    <td align="center"><?php echo $row["Lecturer"]; ?></td>     
        <!-- Adding the delete button for the higher admins -->
                      <?php
        if(isset($_SESSION['specialLog'])){
            echo "<td align='center'><a href = 'includes/DeleteRecord.php?id3=$row[ID]'onclick=\"return confirm('Are you sure you want to delete this record?');\"> Delete </a></td>";
        }
        ?>
        <!-- If these module codes exist, theyre able to be accessed and opened for their content pages -->  
     <?php 
        if($row["Module_Code"] == "CS3AI18"){?>
            <td align="center"><a href="Year 3/CS3AI18.php">More About CS3AI18</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CS3BC20"){?>
            <td align="center"><a href="Year 3/CS3BC20.php">More About CS3BC20</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CS3DP19"){?>
            <td align="center"><a href="Year 3/CS3DP19.php">More About CS3DP19</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CS3DS19"){?>
            <td align="center"><a href="Year 3/CS3DS19.php">More About CS3DS19</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CS3DV20"){?>
            <td align="center"><a href="Year 3/CS3DV20.php">More About CS3DV20</a></td>
           
     <?php } ?>
        
             <?php 
        if($row["Module_Code"] == "CS3IA16"){?>
            <td align="center"><a href="Year 3/CS3IA16.php">More About CS3IA16</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CS3IP16"){?>
            <td align="center"><a href="Year 3/CS3IP16.php">More About CS3IP16</a></td>
           
     <?php } ?>
        
         <?php 
        if($row["Module_Code"] == "CS3PP19"){?>
            <td align="center"><a href="Year 3/CS3PP19.php">More About CS3PP19</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CS3SC17"){?>
            <td align="center"><a href="Year 3/CS3SC17.php">More About CS3SC17</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CS3TM20"){?>
            <td align="center"><a href="Year 3/CS3TM20.php">More About CS3TM20</a></td>
           
     <?php } ?>
       
        <?php 
        if($row["Module_Code"] == "CS3VI18"){?>
            <td align="center"><a href="Year 3/CS3VI18.php">More About CS3VI18</a></td>
           
     <?php } ?>
        
        <?php 
        if($row["Module_Code"] == "CS3VR16"){?>
            <td align="center"><a href="Year 3/CS3VR16.php">More About CS3VR16</a></td>
           
     <?php } ?>
        
  
        
    </tr>
            
 <?php } ?> 
        </table>
      </div>

       
 
    </html>
    </body>