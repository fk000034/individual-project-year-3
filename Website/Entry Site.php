<?php
include_once 'includes/verification.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Opening Page</title>
<link href="Entry.css" rel="stylesheet" type="text/css">
<link rel="icon" href="Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
	
 
   <!-- Using navigation container as an overlay -->
        <div id="myNav" class="overlay">
            <!-- Using javascript function for the navigation system -->
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><div class="rotated"><div class="zoom">&times; </div></div></a>
        
        <div class="overlayC">
            
   <!-- Links to content from menu, when hovered the link is zoomed in, using an animated flow -->
        <a href="FirstYear.php" ><div class="zoom">First Year</div></a>
        <a href="SecondYear.php" ><div class="zoom">Second Year</div></a>
        <a href="ThirdYear.php" ><div class="zoom">Third Year</div></a>
        <a href="Masters.php" ><div class="zoom">Masters</div></a>
        <a href="Statistics.php" ><div class="zoom">Subject Statistics</div></a>
            
        <!-- PHP code to check if user is logged in, if so, one link changes text and another link is added being the create a module link -->
        <?php
            
        if(!isset($_SESSION['login'])){
            echo "<a href='Login Page.php' ><div class='zoom'>Log in</div></a>";
        }
    else{
        
        echo "<a href='LogOutVerify.php' ><div class='zoom'>Log Out</div></a>";
        
    }
        ?>
		            <?php
        if(isset($_SESSION['login'])){
            echo "<a href='Create Module.php' ><div class='zoom'>Add Module</div></a>";
        }
        ?>

    </div>
    </div>
    
    <span style="font-size:70px;cursor:pointer;display:block; height:0px; width: 10px" onclick="openNav()"><div class="clicked"> ☰ </div></span>

<script src="Navigation.js"></script>

<!-- simple header for page -->
        <h1>University of Reading Module Site</h1>
    <!-- checking what user is logged on based on session, user is informed of the login they have, whether they're a higher admin or lower -->
    	            <?php
        if(isset($_SESSION['login']) && !isset($_SESSION['specialLog'])){
            echo "<div class = 'logged'> <p2>Level 1 clearance currently logged in </p2></div>";
        }
    if (isset($_SESSION['specialLog'])){
        echo "<div class = 'logged'> <p2>Level 2 clearance currently logged in</p2></div>";
        }
        ?>
<!-- text for user to read to understand where to start using the site -->
    <div class="textContainer">
        <h2>Click on the ☰ to choose the year of study you wish to obtain information from. </h2>
    </div>
    
       <ul class ="slideshow">
    <li><span>Image 1</span></li>
    <li><span>Image 2</span></li>
    <li><span>Image 3</span></li>
    
    
</ul>
  


    </html>
    </body>