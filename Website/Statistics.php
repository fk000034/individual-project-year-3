<?php
include_once 'includes/verification.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Statistics</title>
<link href="Entry.css" rel="stylesheet" type="text/css">
<link rel="icon" href="Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
    <!-- Using a CDN (Content Delivery Network) -->     
    <script
src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
</script>
<script src="ChartsStatistics.js"></script>
    <!-- loading javascript for the charts -->
    <body onload=Charts()></body>
   <!-- Using navigation container as an overlay -->
        <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><div class="rotated"><div class="zoom">&times; </div></div></a>
        
        <div class="overlayC">
        <!-- Links to content from menu, when hovered the link is zoomed in, using an animated flow -->
            
        <a href="Entry Site.php" ><div class="zoom">Entry Site</div></a>    
        <a href="FirstYear.php" ><div class="zoom">First Year</div></a>
        <a href="SecondYear.php" ><div class="zoom">Second Year</div></a>
        <a href="ThirdYear.php" ><div class="zoom">Third Year</div></a>
        <a href="Masters.php" ><div class="zoom">Masters</div></a>

		            <?php
        if(isset($_SESSION['login'])){
            echo "<a href='Create Module.php' ><div class='zoom'>Add Module</div></a>";
        }
        ?>

    </div>
    </div>
    
    <span style="font-size:70px;cursor:pointer;display:block; height:0px; width: 10px" onclick="openNav()"><div class="clicked"> ☰ </div></span>
<!-- Loading javascripts -->
<script src="Navigation.js"></script>


        <h1>Statistics</h1>
    <div class = "linechartContainer">
      <!-- loading canvas from the javascript file for charts -->
    <canvas id= "lineChart"></canvas>
</div>
        <div class = "piechartContainer">
      
    <canvas id= "pieChartSatisfaction"></canvas>
</div>
  


    </html>
    </body>