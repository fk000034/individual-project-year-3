<?php
include_once 'db.inc.php';
//using post methods to find the user inserted attributes on the form
$module = $_POST["Module"];
$code = $_POST["ModuleCode"];
$category = $_POST["Category"];
$year = $_POST["Year"];
$name = $_POST["lecturer"];

//if the user inserted value is 1, insert into the year 1 table and redirect to year 1 page
if($_POST['Year'] == '1'){
    $sql = "INSERT INTO year1 (Module_Code, Modules, Sub_Category, Year_Of_Study, Lecturer) VALUES ('$code', '$module', '$category', '$year', '$name');";
    mysqli_query($conn, $sql);
    header("Location: ../FirstYear.php");
}//if the user inserted value is 2, insert into the year 2 table and redirect to year 2 page
if($_POST['Year'] == '2'){
  $sql = "INSERT INTO year2 (Module_Code, Modules, Sub_Category, Year_Of_Study, Lecturer) VALUES ('$code', '$module', '$category', '$year', '$name');";
    mysqli_query($conn, $sql);
    header("Location: ../SecondYear.php");
}//if the user inserted value is 3, insert into the year 3 table and redirect to year 3 page
if($_POST['Year'] == '3'){
  $sql = "INSERT INTO year3 (Module_Code, Modules, Sub_Category, Year_Of_Study, Lecturer) VALUES ('$code', '$module', '$category', '$year', '$name');";
    mysqli_query($conn, $sql);
    header("Location: ../ThirdYear.php");
}//if the user inserted value is m or 4, insert into the masters table and redirect to masters page
if($_POST['Year'] == 'm' || $_POST['Year'] == '4' || $_POST['Year'] == 'M'){
  $sql = "INSERT INTO masters (Module_Code, Modules, Sub_Category, Year_Of_Study, Lecturer) VALUES ('$code', '$module', '$category', '$year', '$name');";
    mysqli_query($conn, $sql);
    header("Location: ../Masters.php");
}//if neither the values, error is presented
else{
  echo "Error not submitted, wrong details, year of study must only contain 1, 2, 3, and 4 or M";
}

?>