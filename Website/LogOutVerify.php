<?php
   //resume session
   session_start();
//destroy session so, login session is not running
if(session_destroy()){
    header("Location: Entry Site.php");
    exit;
}
?>