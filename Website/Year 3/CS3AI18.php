<?php
include_once '../includes/verification.php';
?>
<?php
include_once '../includes/db.inc.php';
?>
<?php
   
   $sql = "SELECT*FROM infocontent";
   $result = mysqli_query($conn, $sql);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Artificial Intelligence</title>
<link href="ThirdYearStyle.css" rel="stylesheet" type="text/css">
<link rel="icon" href="../Images/FYP Images/Reading_shield.png">
</head>
<body>
<html>
	
 
   
        <div id="myNav" class="overlay">
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><div class="rotated"><div class="zoom">&times; </div></div></a>
        
        <div class="overlayC">
        
        <a href="../Entry Site.php" ><div class="zoom">Entry Site</div></a>    
        <a href="../FirstYear.php" ><div class="zoom">First Year</div></a>    
        <a href="../SecondYear.php" ><div class="zoom">Second Year</div></a>
        <a href="../ThirdYear.php" ><div class="zoom">Third Year</div></a>
        <a href="../Masters.php" ><div class="zoom">Masters</div></a>
	

    </div>
    </div>
    
   <span style="font-size:70px;cursor:pointer;display:block; height:0px; width: 10px" onclick="openNav()"><div class="clicked"> ☰ </div></span>

<script src="../jquery-3.6.0.min.js"></script>
<script src="../Navigation.js"></script>
<script src="../SavingDetails.js"></script>
 <div class="headerContainer">
    <h1>Artificial Intelligence</h1>
    </div>
 <!-- All taken from Uni of reading module page -->
    
<?php
        if(!isset($_SESSION['login'])){?>
            <?php
              while($row = mysqli_fetch_assoc($result)){?>
              <?php if($row['ID'] == 71) {?>
                     <div class = "infoContainer1">
                     <h2>Requirements:</h2> 
                     <?php echo $row['Content']?></div>
                <?php }?>
              <?php if($row['ID'] == 72) {?>
                     <div class = "infoContainer2" >
                     <h2>Module Information:</h2> 
                     <?php echo $row['Content']?></div>
                <?php }?>
              <?php if($row['ID'] == 73) {?>
                     <div class = "infoContainer3" >
                     <h2>Additional Information:</h2> 
                     <?php echo $row['Content']?></div>
                <?php }?>
              <?php if($row['ID'] == 74) {?>
                     <div class = "infoContainer4" >
                     <h2>Exams/Coursework:</h2> 
                     <?php echo $row['Content']?></div>
                <?php }?>
              <?php if($row['ID'] == 75) {?>
                     <div class = "infoContainer5" >
                     <h2>Course Outline:</h2> 
                     <?php echo $row['Content']?></div>
                <?php }?>
             <?php }?>
        <?php } ?>
           <?php
        if(isset($_SESSION['login'])){?>
          
            <?php
              while($row = mysqli_fetch_assoc($result)){?>
                <?php if($row['ID'] == 71) {?>
                     <div class = "infoContainer1" contenteditable="true" >
                     <h2 contenteditable="false">Requirements:</h2> 
                     <div id="<?php echo $row['ID']?>"><?php echo $row['Content']?></div>
                         <button contenteditable="false" onclick="saveFile('<?php echo $row['ID']?>')"> Save new edits </button>
                         </div> 
                <?php }?>
                <?php if($row['ID'] == 72) {?>
                     <div class = "infoContainer2" contenteditable="true" >
                     <h2 contenteditable="false">Module Information:</h2> 
                     <div id="<?php echo $row['ID']?>"><?php echo $row['Content']?></div>
                         <button contenteditable="false" onclick="saveFile('<?php echo $row['ID']?>')"> Save new edits </button>
                         </div> 
                <?php }?>
                <?php if($row['ID'] == 73) {?>
                     <div class = "infoContainer3" contenteditable="true" >
                     <h2 contenteditable="false">Additional Information:</h2> 
                     <div id="<?php echo $row['ID']?>"><?php echo $row['Content']?></div>
                         <button contenteditable="false" onclick="saveFile('<?php echo $row['ID']?>')"> Save new edits </button>
                         </div> 
                <?php }?>
                <?php if($row['ID'] == 74) {?>
                     <div class = "infoContainer4" contenteditable="true" >
                     <h2 contenteditable="false">Exams/Coursework:</h2> 
                     <div id="<?php echo $row['ID']?>"><?php echo $row['Content']?></div>
                         <button contenteditable="false" onclick="saveFile('<?php echo $row['ID']?>')"> Save new edits </button>
                         </div> 
                <?php }?>
                <?php if($row['ID'] == 75) {?>
                     <div class = "infoContainer5" contenteditable="true" >
                     <h2 contenteditable="false">Course Outline:</h2> 
                     <div id="<?php echo $row['ID']?>"><?php echo $row['Content']?></div>
                         <button contenteditable="false" onclick="saveFile('<?php echo $row['ID']?>')"> Save new edits </button>
                         </div> 
                <?php }?>
             <?php }?>
        <?php } ?>
   
<div class = "backButton"><div class = "zoom"><a href="../ThirdYear.php">Back to page</a></div></div>
       <h3> ...Click this image to go to the official page</h3>
<a href="http://www.reading.ac.uk/module/document.aspx?modP=CS3AI18&modYR=2122" target="_blank"><img src="../Images/FYP Images/Reading_shield.png"></a>
    </html>
    </body>